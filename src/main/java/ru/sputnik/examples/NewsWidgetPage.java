package ru.sputnik.examples;

import net.thucydides.core.pages.PageObject;
import net.thucydides.core.pages.WebElementFacade;
import org.openqa.selenium.By;

import java.util.List;

public class NewsWidgetPage extends PageObject {
    public WebElementFacade russiaTab() { return widgetBlock().find(By.linkText("В России")); }
    public WebElementFacade worldTab() { return widgetBlock().find(By.linkText("В мире")); }
    public WebElementFacade progressTab() { return widgetBlock().find(By.linkText("Прогресс")); }
    public WebElementFacade sportTab() { return widgetBlock().find(By.linkText("Спорт")); }
    public WebElementFacade carsTab() { return widgetBlock().find(By.linkText("Авто")); }

    public List<WebElementFacade> newsLinks() {
        return widgetBlock().thenFindAll(By.cssSelector("ul.b__show a.b-w-news-list__link"));
    }

    private WebElementFacade widgetBlock() { return element(By.cssSelector("div[data-name=news]")); }
}
