package ru.sputnik.examples;

import net.thucydides.core.annotations.*;
import net.thucydides.core.pages.Pages;
import net.thucydides.junit.runners.ThucydidesRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

/**
 * Функциональные тесты новостного виджета (пример).
 */
@RunWith(ThucydidesRunner.class)
public class NewsWidgetTest {
    @Managed
    WebDriver webDriver;

    @ManagedPages
    Pages pages;

    @Steps
    NewsWidgetSteps newsWidgetSteps;

    @Test
    @Title("Пользователю должны показываться 5 последний новостей")
    public void widgetShouldRender5RecentNews() {
        newsWidgetSteps.openMainPage();
        newsWidgetSteps.checkThatRenderedNNewsLinks(5);
    }

    @Test
    @Title("Пользователю должны показываться 5 последних новостей по России")
    public void widgetShouldRender5RecentNewsInRussia() {
        newsWidgetSteps.openMainPage();
        newsWidgetSteps.switchToNewsInRussia();
        newsWidgetSteps.checkThatRenderedNNewsLinks(5);
    }

    @Test
    @Title("Пользователю должны показываться 5 последних новостей в мире")
    public void widgetShouldRender5RecentNewsInWorld() {
        newsWidgetSteps.openMainPage();
        newsWidgetSteps.switchToNewsInWorld();
        newsWidgetSteps.checkThatRenderedNNewsLinks(5);
    }

    @Test
    @Title("Пользователю должны показываться 5 последних новостей о прогрессе")
    public void widgetShouldRender5RecentNewsAboutProgress() {
        newsWidgetSteps.openMainPage();
        newsWidgetSteps.switchToNewsAboutProgress();
        newsWidgetSteps.checkThatRenderedNNewsLinks(5);
    }

    @Test
    @Title("Пользователю должны показываться 5 последних новостей о спорте")
    public void widgetShouldRender5RecentNewsAboutSport() {
        newsWidgetSteps.openMainPage();
        newsWidgetSteps.switchToNewsAboutSport();
        newsWidgetSteps.checkThatRenderedNNewsLinks(5);
    }

    @Test
    @Title("Пользователю должны показываться 5 последних новостей о автомобилях")
    public void widgetShouldRender5RecentNewsAboutCars() {
        newsWidgetSteps.openMainPage();
        newsWidgetSteps.switchToNewsAboutCars();
        newsWidgetSteps.checkThatRenderedNNewsLinks(5);
    }
}
