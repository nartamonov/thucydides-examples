package ru.sputnik.examples;

import net.thucydides.core.annotations.*;
import net.thucydides.core.pages.Pages;
import net.thucydides.junit.runners.ThucydidesRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

/**
 * Функциональные тесты поиска по интернету (пример).
 */
@RunWith(ThucydidesRunner.class)
public class InternetSearchTest {
    @Managed
    WebDriver webDriver;

    @ManagedPages
    Pages pages;

    @Steps
    InternetSearchSteps searchSteps;

    @Test
    @Title("При вводе поисковой фразы 'путин' должны показываться подсказки двух специальных типов")
    public void testSearchHintsOnPutinPhrase() {
        searchSteps.openMainPage();
        searchSteps.enterSearchPhrase("путин");
        searchSteps.checkWikiHint();
        searchSteps.checkMarkerHint();
    }

    @Test
    @Title("При вводе поисковой фразы 'путин' должены показываться 12 подсказок")
    public void testThatTotalSearchHintsIs12() {
        searchSteps.openMainPage();
        searchSteps.enterSearchPhrase("путин");
        searchSteps.checkThatRenderedNHints(12);
    }
}
