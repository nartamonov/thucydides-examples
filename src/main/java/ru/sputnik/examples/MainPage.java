package ru.sputnik.examples;

import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.findby.FindBy;
import net.thucydides.core.pages.PageObject;
import net.thucydides.core.pages.WebElementFacade;
import org.openqa.selenium.By;

import java.util.List;

@DefaultUrl("http://sputnik.ru")
public class MainPage extends PageObject {
    public NewsWidgetPage newsWidget;

    @FindBy(css="input#search-form")
    WebElementFacade searchInput;

    @FindBy(css="div.b-hints")
    WebElementFacade hintsBlock;

    @FindBy(css="div.b-hints li.b-hints__item_wiki")
    WebElementFacade wikiHint;

    @FindBy(css="div.b-hints li.b-hints__item_marker")
    WebElementFacade markerHint;

    public List<WebElementFacade> allHints() {
        return hintsBlock.thenFindAll(By.cssSelector("li.b-hints__item"));
    }
}
