package ru.sputnik.examples;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Title;
import net.thucydides.core.steps.ScenarioSteps;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class NewsWidgetSteps extends ScenarioSteps {
    MainPage mainPage;

    @Step
    public void openMainPage() {
        mainPage.open();
    }

    @Step(value = "Перейти к новостям в России")
    public void switchToNewsInRussia() {
        mainPage.newsWidget.russiaTab().click();
    }

    @Step(value = "Перейти к новостям в мире")
    public void switchToNewsInWorld() {
        mainPage.newsWidget.worldTab().click();
    }

    @Step(value = "Перейти к новостям о прогрессе")
    public void switchToNewsAboutProgress() {
        mainPage.newsWidget.progressTab().click();
    }

    @Step(value = "Перейти к новостям о спорте")
    public void switchToNewsAboutSport() {
        mainPage.newsWidget.sportTab().click();
    }

    @Step(value = "Перейти к новостям о автомобилях")
    public void switchToNewsAboutCars() {
        mainPage.newsWidget.carsTab().click();
    }

    @Step(value = "Проверить, что отображается N новостных ссылок")
    public void checkThatRenderedNNewsLinks(int n) {
        assertThat(mainPage.newsWidget.newsLinks().size(), equalTo(n));
    }
}
