package ru.sputnik.examples;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class InternetSearchSteps extends ScenarioSteps {
    MainPage mainPage;

    @Step
    public void openMainPage() {
        mainPage.open();
    }

    @Step(value = "Ввести поисковую фразу")
    public void enterSearchPhrase(String phrase) {
        mainPage.searchInput.sendKeys(phrase);
    }

    @Step(value = "Проверить, что отображается спец. поисковая подсказка с отсылкой на википедию")
    public void checkWikiHint() {
        mainPage.wikiHint.shouldBePresent();
        mainPage.wikiHint.shouldBeVisible();
    }

    @Step(value = "Проверить, что отображается спец. поисковая подсказка с отсылкой на сайт")
    public void checkMarkerHint() {
        mainPage.markerHint.shouldBePresent();
        mainPage.markerHint.shouldBeVisible();
    }

    @Step(value = "Проверить, что отображается N поисковых подсказок")
    public void checkThatRenderedNHints(int n) {
        assertThat(mainPage.allHints().size(), equalTo(n));
    }
}
